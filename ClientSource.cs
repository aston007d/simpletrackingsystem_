﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.ComponentModel;
using System.Diagnostics;

namespace Client
{
    class ClientSource
    {
        private static TcpClient tcpClient = new TcpClient();
        private static BackgroundWorker bw = new BackgroundWorker();
        private static string remoteIp = "127.0.0.1"; //Type your IP
        private static int scount = 0, ccTop = 0;
        private static int port = 8001;        
        private static volatile bool runNext = true;
        private static AutoResetEvent autoReset = new AutoResetEvent(false);

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
            Console.CancelKeyPress += Console_CancelKeyPress;
            ccTop = Console.CursorTop;

            bw.WorkerSupportsCancellation = true;
            bw.WorkerReportsProgress = true;
            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            bw.RunWorkerAsync();

            while (runNext)
            {
                Console.ReadKey(true);
            }
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            e.Cancel = true;
            bw.CancelAsync();
            Dispose();
        }

        private static void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
                Print($"Error in transmission thread: {e.Error.Message}", ccTop, newLine: true);
            Console.WriteLine("Transmission stoped. Press any key to exit.");
            Dispose();
            bw.Dispose();
        }

        private static void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            if (!TryConnect())          
                return;
             
            while (!worker.CancellationPending && tcpClient.Connected)
            {
                try
                {
                    SendScreenShot();
                    Thread.Sleep(2000);
                }
                catch(Exception)
                {
                    return;
                }

                Console.WriteLine();
            }
        }

        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            Dispose();
            bw.Dispose();
        }

        private static void Dispose()
        {
            runNext = false;
            tcpClient.Close();
            tcpClient.Dispose();
        }

        private static byte[] GetScreenShot()
        {
            #region
            //string path = Path.GetTempPath() + @"screens\";
            //string sPath;

            //if (!Directory.Exists(path))
            //{
            //    try
            //    {
            //        Directory.CreateDirectory(path);
            //    }
            //    catch (Exception e)
            //    {                   
            //        throw new Exception(string.Format("Unable to create directory: {0}", path), e);
            //    }
            //}
            #endregion
            ///
            ///Returning array has the following format: byte[imageSize + image]
            /// imageSize is a byte array converted from Int32 image bytes count. It's size equals to sizeof(Int32).
            /// image is a byte array contains byte representation of screenshot.
            /// 

            try
            {
                Bitmap bitmap = new Bitmap(Screen.PrimaryScreen.Bounds.Width,
                                           Screen.PrimaryScreen.Bounds.Height);
                Graphics graphics = Graphics.FromImage(bitmap as Image);
                graphics.CopyFromScreen(0, 0, 0, 0, bitmap.Size);


                byte[] ba = new ImageConverter().ConvertTo(bitmap, 
                                                        typeof(byte[])) as byte[];
                byte[] sl = BitConverter.GetBytes(ba.Length);
                byte[] res = new byte[ba.Length + sl.Length];
                Array.Copy(sl, 0, res, 0, sl.Length);
                Array.Copy(ba, 0, res, sl.Length, ba.Length);

                return res;

            }
            catch (Exception e)
            {
                throw new Exception("Some problems with creating screenshot.", e);
            }
        }

        private static void SendScreenShot()
        {
            try
            {
                byte[] s = GetScreenShot();
                var stream = tcpClient.GetStream();
                stream.Write(s, 0, s.Length);
            }
            catch (System.IO.IOException ex)
            {
                //Console.WriteLine("Server dropped connection. Message: {0}", ex.Message);
                Console.WriteLine("\nServer dropped connection");
                //Print("Server dropped connection", ccTop, newLine: true);
                throw;
            }
            catch (ObjectDisposedException ex)
            {
                //Console.WriteLine("Attempt to read data from closed TCPClient", ex.Message);
                Console.WriteLine("\nAttempt to read data from closed TCPClient");
                //Print("Attempt to read data from closed TCPClient", ccTop, newLine: true);
                throw;
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.Message);
                Print($"{e.Message}", ccTop);
                return;
            }

            //Console.Write("\rSent {0} screenshots   ", ++scount);
            Print($"Sent {++scount} screenshots", ccTop);
        }

        private static bool TryConnect(string remoteIp = "127.0.0.1", int port = 8001, int rCount = 3)
        {

            //Console.WriteLine("Connecting...");
            Print("Connecting...", ccTop);
            for (int i = 1; i < rCount + 1; ++i)
            {
                try
                {
                    tcpClient.Connect(remoteIp, port);
                    //Console.WriteLine("Connected.");
                    Print("Connected.", ccTop, newLine: true);
                    return true;
                }
                catch (SocketException exc)
                {
                    if (exc.SocketErrorCode == SocketError.TimedOut)
                    {
                        //Console.WriteLine("Failed to connect. Retrying {0}", i);
                        Print(string.Format("Failed to connect. Retrying {0}", i), ccTop);
                        continue;
                    }
                    if (exc.SocketErrorCode == SocketError.NotSocket)
                    {
                        return false;
                    }

                    //Console.WriteLine($"Connection error: {exc.Message}");
                    Print($"Connection error", ccTop, newLine: true);
                    return false;
                }
                catch (Exception ex)
                {
                    //Console.WriteLine(ex.Message);
                    Print($"{ex.Message}", ccTop, newLine: true);
                    return false;
                }
            }

            //Console.WriteLine("Server does not respronce.");
            Print("Server does not respronce.", ccTop, newLine: true);
            return false;
        }

        public static void Print(string format, int cTop, int cLeft = 0, bool newLine = false)
        {
                Console.SetCursorPosition(cLeft, cTop);
                Console.Write(new string(' ', Console.WindowWidth - cLeft));
                Console.SetCursorPosition(cLeft, cTop);
                Console.Write(format);
                if (newLine)
                {
                    ccTop++;
                    Console.CursorTop++;
                }
        }
    }
}
